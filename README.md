# Drupal Security Check

This is a simple Drush script to automate checking multiple Drupal sites for security updates, and reporting their status back to you in the console or via email.

## Requirements

This script requires Drush 6. Drush 5 will not work.

## Configuration

Before running the script, you need to copy `example.config.info` and rename it to, for example, `config.info`. You can keep it in the same directory as this script or place it somewhere else.

### Create a config file

Specify the sites you want to check in the config. The sites must have valid Drush aliases defined. Leave off the `@` when specifying a site. For example, if you have Drush aliases for local, staging, and production environments for `@example`, and you want to check the staging and production environments for security updates you would specify:

```
aliases[] = example.stage
aliases[] = example.prod
```

Note: the aliases must contain the `root` and `uri` keys.

#### (Optional) Set mail options

If you want to have the security update report sent to you via email, you can specify the following mail options:

```
; Mailing options
mail[from] = you@example.com
mail[to] = someone@example.com
mail[subject] = "Security updates status report - from Server A"
; Mail the report only if there are security updates.
mail[errors_only] = 1
```

This is useful if you want to regularly run the security check via a cronjob and have the results mailed to you when there are security updates for a site.

## Usage

To run a security update report, you can type:

`drush scr /path/to/security-check.php`

If the config file you created is not in the same directory as the `security-check.php` script, you will need to specify the path to the config file. For example:

`drush scr /path/to/security-check.php --config=/path/to/config/file`

If you want the report emailed to you, make sure you've set the email options in the config, and append the `--mail` option to your command:

`drush scr /path/to/security-check.php --mail`
