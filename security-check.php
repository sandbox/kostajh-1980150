#!/usr/bin/env drush

<?php

/**
 * @file
 * Checks remote sites for security updates.
 */

define(NEW_LINE, "\r\n");

/**
 * This script checks remote sites for security updates.
 *
 * Usage:
 *
 *  `drush scr security-check`
 *
 *  `drush scr security-check --mail
 *
 *  `drush scr security-check --mail --config=/path/to/config
 */

if (DRUSH_MAJOR_VERSION < 6) {
  return drush_set_error('This script can only be run with Drush 6.');
}

$send = drush_get_option('mail');
$config_file = drush_get_option('config');
if (!$config_file) {
  $config_file = getcwd() . '/config.info';
}
if (!file_exists($config_file)) {
  return drush_set_error(dt('No config file found at !path', array('!path' => $config_file)));
}
$config = parse_ini_file($config_file);
if (!$config) {
  return drush_set_error(dt('Could not parse config file at !path', array('!path' => $config_file)));
}

$aliases_to_check = $config['aliases'];

$mail_conf = isset($config['mail']) ? $config['mail'] : array();
$mailer = new Mailer($send, $mail_conf);

$ret = drush_invoke_process('@self', 'site-alias', array(), array(), array('integrate' => FALSE));

// Remove '@' from aliases if it is present.
$alias_output = preg_replace('/@/', '', $ret['output']);
$local_aliases = explode("\n", $alias_output);
$aliases = array();

foreach ($aliases_to_check as $alias) {
  if (in_array($alias, $local_aliases)) {
    $aliases[$alias] = '@' . $alias;
  }
  else {
    $output = dt('The alias !alias was not found in your ~/.drush directory and will not be checked!',
      array('!alias' => $alias));
    $mailer->appendContent($output);
    drush_log($output, 'warning');
  }
}

$output = dt('The following !count aliases were found and will be checked for security updates: ',
  array('!count' => count($aliases)));
$mailer->appendContent($output);
drush_log($output, 'ok');


foreach ($aliases as $key => $alias) {
  $output = dt('- !alias', array('!alias' => $alias));
  $mailer->appendContent($output);
  drush_log($output, 'ok');
}

if (!drush_confirm('Would you like to proceed?')) {
  return;
}

$begin_status_check = time();
$security_updates = array();
$all_clear = array();

$count = 0;
$total = count($aliases);

foreach ($aliases as $key => $alias) {
  drush_print();
  $mailer->appendContent(NEW_LINE);
  $output = dt('Checking !alias for security updates', array('!alias' => $alias));
  $mailer->appendContent($output);
  drush_log($output, 'ok');
  $php = "";
  $drush = "drush";

  $site = drush_invoke_process('@self', 'site-alias', array($alias), array(), array('integrate' => FALSE));

  if (isset($site['object'][$key]['php'])) {
    $php = $site['object'][$key]['php'] . " ";
  }
  if (isset($site['object'][$key]['path-aliases']['%drush-script'])) {
    $drush = $site['object'][$key]['path-aliases']['%drush-script'];
  }
  // Check if site exists locally.
  if (file_exists($site['object'][$key]['root'])) {
    $cmd = sprintf("%s --root=%s up --security-only --simulate --no", $drush, $site['object'][$key]['root']);
  }
  else {
    $cmd = drush_shell_proc_build($site['object'][$key], sprintf('%s%s up --security-only --simulate --no', $php, $drush), TRUE);
  }
  if (drush_shell_exec($cmd)) {
    $output = drush_shell_exec_output();
    foreach ($output as $line) {
      if (strpos($line, 'SECURITY UPDATE') > 0) {
        $security_updates[$alias][] = $line;
      }
    }
  }
  else {
    $output = dt('Failed to obtain project security update info for site !site.', array('!site' => $alias));
    $mailer->appendContent($output);
    drush_set_error($output);
  }
  // Show info about the updates.
  if (isset($security_updates[$alias])) {
    $output = dt("Found security update(s) for site !alias!",
          array('!alias' => $alias));
    $mailer->appendContent($output);
    drush_log($output, 'error');
    foreach ($security_updates[$alias] as $error) {
      $output = dt('- !error', array('!error' => $error));
      $mailer->appendContent($output);
      drush_log($output, 'error');
    }
  }
  else {
    $all_clear[$alias] = $alias;
  }
  $count++;
  $percent = (double) ($count / $total);
  $output = dt('Done checking !alias. !count of !total sites checked (!percent%).',
    array(
      '!alias' => $alias,
      '!percent' => number_format($percent * 100, 0),
      '!count' => $count,
      '!total' => count($aliases),
    ));
  $mailer->appendContent($output);
  drush_log($output, 'success');

}

drush_print();
$mailer->appendContent(NEW_LINE);
if ($security_updates) {
  $output = 'The following sites have pending security updates!';
  $mailer->appendContent($output);
  drush_log($output, 'warning');
  foreach ($security_updates as $alias => $update) {
    $output = dt('- !alias', array('!alias' => $alias));
    $mailer->appendContent($output);
    drush_log($output, 'error');
  }
}
else {
  $output = 'No security updates pending for your sites!';
  $mailer->appendContent($output);
  drush_log($output, 'success');
}

if ($all_clear) {
  drush_print();
  $mailer->appendContent(NEW_LINE);
  $output = 'The following sites are all up to date.';
  $mailer->appendContent($output);
  drush_log($output, 'success');
  foreach ($all_clear as $alias) {
    $output = dt('- !alias', array('!alias' => $alias));
    $mailer->appendContent($output);
    drush_log($output, 'success');
  }
}

$end_status_check = time();

$total_status_check_time = $end_status_check - $begin_status_check;
drush_print();
$mailer->appendContent(NEW_LINE);
$output = dt('Status check completed in !min minute(s) and !sec second(s).',
        array(
          '!min' => gmdate("i", $total_status_check_time),
          '!sec' => gmdate("s", $total_status_check_time))
          );
$mailer->appendContent($output);
drush_log($output, 'success');

if ($config['errors_only'] == 1 && !$security_updates) {
  return TRUE;
}

if ($send && $mailer->sendMail()) {
  drush_log('Sent notification mail.', 'success');
}
else {
  drush_set_error('Failed to send notification mail!');
}

/**
 * Handles building mail content and sending alert.
 */
class Mailer {

  public static $content = '';

  /**
   * Constructor.
   */
  public function __construct($send = FALSE, $mail_conf = array()) {
    $this->send = $send;
    $this->config = $mail_conf;
  }

  /**
   * Append content to mail output.
   */
  public function appendContent($output) {
    self::$content .= $output . NEW_LINE;
  }

  /**
   * Return the content that has been assembled.
   */
  public function getContent() {
    return self::$content;
  }

  /**
   * Send the notification.
   */
  public function sendMail() {
    $content = self::$content;
    $from = $this->config['from'];
    $to = $this->config['to'];
    $subject = $this->config['subject'];
    $headers = sprintf('From: %s', $from) . NEW_LINE . sprintf('Reply-To: %s', $from) . NEW_LINE . 'X-Mailer: PHP/' . phpversion();
    return mail($to, $subject, $content, $headers);
  }
}
